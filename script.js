const hamburger = document.getElementsByClassName('hamburger')[0]
const links = document.getElementsByClassName('links')[0]

hamburger.addEventListener('click', () => {
    links.classList.toggle('active')
})

